\section{Theoretical Background}

\begin{frame}{Gaussian Processes}

    \pause

    \begin{definition}
        Let $\mathbb{X}$ be a (generally infinite) set, referred to as the "input space". For every $x$ in $\mathbb{X}$, let $f\left(x\right)$ be a random variable. The collection $f\left(x\right)$ is said to be a \emph{Gaussian process} (GP) if any finite sub-collection of $f\left(x\right)$ has a joint Gaussian distribution.
    \end{definition}

    \pause

    A GP is completely specified by its mean $m\left(x\right)$ and covariance $K\left(x,y\right)$ (known as the \emph{kernel}) functions, which are defined as

    \begin{align}
        m\left(x\right)   & =\mathbb{E}[f\left(x\right)]                                                                                     \\ \nonumber
        K\left(x,y\right) & =\mathbb{E}\left[\left(f\left(x\right)-m\left(x\right)\right)\left(f\left(y\right)-m\left(y\right)\right)\right]
    \end{align}

    \pause

    A GP is denoted as $f\sim \mathcal{GP}\left(m,K\right)$. Often $m\left(x\right)=0$, so

    \begin{equation}
        \label{Eq:kernelDef}
        K\left(x,y\right)=\mathbb{E}\left[f\left(x\right)f\left(y\right)\right]
    \end{equation}

    In the context of this thesis, $\mathbb{X}$ is always taken to be $S^{d-1}$.

\end{frame}

\begin{frame}{Bayesian Prediction on Gaussian Processes I}

    \pause

    Let us assume that we try to learn a target function $g$.

    \pause

    Given some new "information" $D=\{\mathcal{X},\mathcal{Y},\sigma^2\}$, the prediction is given by

    \begin{equation}
        \label{Eq:matrixInverse}
        g^*\left(x_*\right)=\sum_{n,m} K\left(x_*,x_n\right) \left[K\left(\mathcal{X}\right) + \sigma^2I \right]^{-1}_{nm}g\left(x_m\right)
    \end{equation}

    where $K\left(\mathcal{X}\right)_{nm}=K(x_n,x_m)$ is the kernel projected on the dataset, and $I$ is the $N\times N$ identity matrix.

\end{frame}

\begin{frame}{Bayesian Prediction on Gaussian Processes II}

    \pause

    It is also known that the prediction $g^*$ is the function that minimizes the following energy functional

    \begin{equation}
        \label{Eq:functional}
        E\left[f\right] =\frac{1}{2}\left\Vert f\right\Vert_K^2+\frac{1}{2\sigma^2}\sum_{x_i\in \mathcal{X}}\left(f\left(x_i\right)-g\left(x_i\right)\right)^2
    \end{equation}

    where $\left\Vert f\right\Vert_K^2$ is the RKHS norm, which for some probability measure $d\mu=P\left(x\right)dx$ over the input space $\mathbb{X}$, is defined as

    \begin{equation}
        \left\Vert f\right\Vert_K^2=\intop_{x\in\mathbb{X}}\intop_{y\in\mathbb{X}} d\mu_x d\mu_y f\left(x\right) K^{-1}\left(x,y\right)f\left(y\right)
    \end{equation}

    where $K^{-1}$ is the inverse kernel, meaning that

    \begin{equation}
        \intop_{\xi\in\mathbb{X}} d\mu_\xi K(x,\xi)K^{-1}(\xi,y)=\delta\left(x-y\right)/P\left(x\right)
    \end{equation}

\end{frame}

\begin{frame}{Physical Analogy I}

    \pause

    Consider a $d-1$ dimensional hyperspherical fluctuating elastic membrane. Its elastic energy is given by the non-local energy functional

    \begin{equation}
        E\left[f\right] =\underbrace{\frac{1}{2}\left\Vert f\right\Vert_K^2}_{\text{Elasticity term}}+\underbrace{\frac{1}{2\sigma^2}\sum_{x_i\in \mathcal{X}}\left(f\left(x_i\right)-g\left(x_i\right)\right)^2}_{\text{Data term (disorder)}}
    \end{equation}

    where $f$ is the membrane's displacement. The data term can be thought of as springs of stiffness $1/\sigma^2$.

    \pause

    Assuming that the system is at a temperature $\beta=1$, the Boltzmann distribution over the space of displacement functions is given by

    \begin{equation}
        p\left[f\right]\propto \exp\left(- E\left[ f \right]\right)
    \end{equation}

    On average, the membrane relaxates towards its average

    \begin{equation}
        g^*=\arg\min E\left[ f \right]
    \end{equation}

\end{frame}

\begin{frame}{Physical Analogy II}

    \begin{figure}
        \centerline{\includegraphics[scale=.5]{assets/fluctuating_membrane.png}}
        \caption{A fluctuating membrane.}
    \end{figure}

\end{frame}

\begin{frame}{Learning Curves I}

    \pause

    The quality of a learning model (and specifically a GP regression) is usually quantified by the \emph{generalization error} (GE)

    \begin{equation}
        \label{Eq:GED}
        \mathrm{Err}(g,\mathcal{X})=\intop_{x\in  \mathbb{X}}d\mu_x \left(g^*\left(x\right)-g\left(x\right)\right)^2
    \end{equation}

    where the r.h.s depends on $\mathcal{X}$ implicitly through $g^*$.

    \pause

    $\mathcal{X}$ specific -- Too restricted. Averaging over the disorder:

    \begin{equation}
        \label{Eq:learningCurveDef}
        \mathrm{Err}\left(g,N\right)=\left\langle \mathrm{Err}(g,\mathcal{X})\right\rangle_{\mathcal{X}\sim \mu^N}= \intop_{x\in  \mathbb{X}}d\mu_x \left\langle\left(g^*\left(x\right)-g\left(x\right)\right)^2\right\rangle_{\mathcal{X}\sim \mu^N}
    \end{equation}

    so we want $\left\langle g^*\left(x\right)\right\rangle_{\mathcal{X}\sim \mu^N}$ and $\left\langle {g^{*}}^{2}\left(x\right)\right\rangle_{\mathcal{X}\sim \mu^N}$ .

    The relation between $\mathrm{Err}\left(g,N\right)$ and $N$ is known as the \emph{learning curve}, and it describes the average rate at which the GP learns $g$.

\end{frame}

\begin{frame}{Learning Curves II}

    \pause

    \begin{block}{Why are learning curves interesting?}
        They encompasses the rate of which the model learns different functions, and may unravel the "natural tendency" of the model towards specific functions.
    \end{block}

    \pause

    So, if $g^*$ is given in Eq. \eqref{Eq:matrixInverse}, what is missing? Whay can't we simply calculate Eq. \eqref{Eq:learningCurveDef}?

    \begin{itemize}
        \item\pause Inversion of a $N\times N$ matrix ($N$ is often $\sim 10^4$ or more)
        \item\pause Averaging over all possible datasets
        \item\pause Not analytically clear
    \end{itemize}

\end{frame}

\begin{frame}{The Equivalence Kernel I}

    \pause

    A common way of approximating the prediction (and therefore the learning curve) is to replace the sum in \eqref{Eq:functional} with an integral, which results in the approximated functional

    \begin{equation}
        \label{Eq:approxFunctional}
        \frac{1}{2}\left\Vert f\right\Vert_K^2+\frac{N}{2\sigma^2}\intop_{x\in\mathbb{X}} d\mu_x\left(f\left(x\right)-g\left(x\right)\right)^2
    \end{equation}

    \pause

    This approximation is known as the \emph{equivalence kernel} (EK).

    \pause
    Minimizing the above functional we get

    \begin{equation}
        \label{Eq:EK}
        g^*_{EK,N}\left(x_*\right)=\sum_i \frac{\lambda_i}{\lambda_i+\frac{\sigma^2}{N}}g_i\phi_i\left(x_*\right)
    \end{equation}

    for $g\left(x\right)=\sum_i g_i\phi_i\left(x\right)$.

\end{frame}

\begin{frame}{The Equivalence Kernel II}

    \begin{equation}
        \nonumber
        g^*_{EK,N}\left(x_*\right)=\sum_i \frac{\lambda_i}{\lambda_i+\frac{\sigma^2}{N}}g_i\phi_i\left(x_*\right)
    \end{equation}

    \pause

    By substituting the above approximation for $g^*$ inside \eqref{Eq:learningCurveDef}, one may approximate the learning curve in an analytically tractable manner.

    \pause

    \begin{block}{Roughly speaking...}
        GP regression fits the kernel's eigenfunctions with larger eigenvalues ($\lambda_i \gg \frac{\sigma^2}{N}$) to the target function, where eigenfunctions with lower eigenvalues are invoked as larger datasets are applied.
    \end{block}

    \pause

    Why is it not good enough?

    \begin{itemize}
        \item\pause No tangible meaning for the assumptions of the approximation
        \item\pause Uncontrolled - Impossible to refine iteratively
    \end{itemize}

\end{frame}

\begin{frame}{Fully Connected Neural Networks I}

    \pause

    Just like GPs, \emph{fully connected neural networks} (FCNNs) are a common model for regression.

    \pause

    \begin{figure}[h]
        \centering

        %%%%%%%%%%%%%%5
        \def\layersep{2.5cm}

        \begin{tikzpicture}[shorten >=1pt,->,draw=black!50, node distance=\layersep,scale=0.9, transform shape]
            \tikzstyle{every pin edge}=[<-,shorten <=1pt]
            \tikzstyle{neuron}=[circle,fill=black!25,minimum size=17pt,inner sep=0pt]
            \tikzstyle{input neuron}=[neuron, fill=green!50];
            \tikzstyle{output neuron}=[neuron, fill=red!50];
            \tikzstyle{hidden neuron}=[neuron, fill=blue!50];
            \tikzstyle{annot} = [text width=4em, text centered]

            % Draw the input layer nodes
            \foreach \name / \y in {1,...,4}
            % This is the same as writing \foreach \name / \y in {1/1,2/2,3/3,4/4}
            \node[input neuron, pin=left:Input \#\y] (I-\name) at (0,-\y) {};

            % Draw the hidden layer nodes
            \foreach \name / \y in {1,...,5}
            \path[yshift=0.5cm]
            node[hidden neuron] (H-\name) at (\layersep,-\y cm) {};

            % Draw the output layer node
            \node[output neuron,pin={[pin edge={->}]right:Output}, right of=H-3] (O) {};

            % Connect every node in the input layer with every node in the
            % hidden layer.
            \foreach \source in {1,...,4}
            \foreach \dest in {1,...,5}
            \path (I-\source) edge (H-\dest);

            % Connect every node in the hidden layer with the output layer
            \foreach \source in {1,...,5}
            \path (H-\source) edge (O);

            % Annotate the layers
            \node[annot,above of=H-1, node distance=1cm] (hl) {Hidden layer};
            \node[annot,left of=hl] {Input layer};
            \node[annot,right of=hl] {Output layer};
        \end{tikzpicture}

        %%%%%%%%%%%%
        \caption{A FCNN Example}
    \end{figure}

\end{frame}

\begin{frame}{Fully Connected Neural Networks II}

    \begin{figure}[h]
        \centering
        \begin{tikzpicture}[scale=0.7, transform shape]

            \begin{scope}[start chain=1]
                \node[on chain=1,draw,circle,join=by o-latex]  at (-1,1.7cm) (a) {$h^{\left(l\right)}_{1}$};
            \end{scope}

            \begin{scope}[start chain=2]
                \node[on chain=2,draw,circle,join=by o-latex]  at (-1,0.5cm) (b) {$h^{\left(l\right)}_{2}$};
            \end{scope}

            \begin{scope}[start chain=6]
                \node[on chain=6,join=by o-latex] at (1,0cm)(sigma) {};
            \end{scope}

            \begin{scope}[start chain=3]
                \node[on chain=3,join=by -latex,label=above:{\parbox{2cm}{\centering Neuron's\\ Input}}] at (2.8,0cm) {$b^{\left(l\right)}_i+\sum_{j=1}^{N_l}W^{\left(l\right)}_{i,j}h^{\left(l\right)}_{j}$};
                \node[on chain=3,draw,circle,join=by -latex,label=above:{\parbox{2cm}{\centering Activation\\ Function}}]   {$\phi$};
                \node[on chain=3,join=by -latex,label=above:{\parbox{2cm}{\centering Neuron's\\ Output}},join=by -latex]{ $h^{(l+1)}_{i}= \phi\left(b^{\left(l\right)}_i+\sum_{j=1}^{N_l}W^{\left(l\right)}_{i,j}h^{\left(l\right)}_{j}\right)$};
                \node[on chain=3, join=by -latex]{};
            \end{scope}

            \begin{scope}[start chain=4]
                \node[on chain=4,join=by o-latex] at (-1,-0.4cm)
                {$\vdots$};
            \end{scope}

            \begin{scope}[start chain=5]
                \node[on chain=5,draw,circle,join=by o-latex] at (-1,-1.5cm)
                (c) {$h^{\left(l\right)}_{N_l}$};
            \end{scope}

            % Arrows joining w1, w3 and b to sigma
            \draw[-latex] (a) -- (sigma);
            \draw[-latex] (b) -- (sigma);
            \draw[-latex] (c) -- (sigma);


        \end{tikzpicture}
        \caption{The action of a single neuron. This scheme shows the data processing performed by the $i$th neuron of layer $l+1$.}
        \medskip
        \small

        \label{fig:singleNeuron}
    \end{figure}

    \pause

    \begin{block}{Training}
        Iterative (usually stochastic) algorithm for finding the optimal weights and biases by minimizing an error functional.
    \end{block}

\end{frame}

\begin{frame}{Equivalence with Gaussian Processes}

    \pause

    \begin{block}{DNN--GP correspondence}
        A set of facilitating assumptions over the DNN structure and training protocol, under which the prediction becomes a GP.
    \end{block}

    \pause

    It was shown (O. Ben-David, Z. Ringel, 2019) that for a specific training protocol, DNNs become a stochastic process such that

    \begin{align}
        P\left[f\right] \propto P_{nd}\left[f\right] e^{-\frac{1}{2\sigma^2}\mathcal{L}\left[f\right] }
    \end{align}

    where $\mathcal{L}$ is the loss function, and $P_{nd} \left[ f \right]$ is the DNN trained with no data.

    \pause

    For infinitely wide DNNs, $P_{nd}$ becomes Gaussian (CLT).

\end{frame}
