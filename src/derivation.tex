\section{Derivation}

\begin{frame}{Phrasing the Problem as a Field Theory Problem}

    \pause

    The posterior partition function (with an added source term):

    \begin{equation}
        \label{Eq:Z_GC}
        \mathcal{Z}\left[\alpha\right]=\int\mathcal{D} f\exp\left({-\frac{1}{2}\left\Vert f\right\Vert_K^2} - \frac{1}{2\sigma^2}\sum_{i=1}^{N}\left(g\left(x_i\right)-f\left(x_i\right)\right)^2 +\int dx \alpha\left(x\right)f\left(x\right)\right)
    \end{equation}

    The exponent is quadratic in $f$ leading to a Gaussian distribution over the space of functions. \pause Indeed, for the mean we get

    \begin{gather}
        \label{Eq:Zprediction}
        \left\langle f\left(x_*\right) \right\rangle =\left.\frac{\delta\log(\mathcal{Z}\left[\alpha\right])}{\delta\alpha\left(x_*\right)}\right|_{\alpha=0}=\\ \nonumber =\left.\arg\min\left[\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}+\frac{1}{2\sigma^2}\sum_{i=1}^N\left(f\left(x_i\right)-g\left(x_i\right) \right)^2\right]\right|_{x_*}=g^*\left(x_*\right)
    \end{gather}

    since for Gaussian distributions it holds that the average case is also the most probable case, in complete correspondence with Eq. \eqref{Eq:functional}.

\end{frame}

\begin{frame}{Calculating Observables I}

    \pause

    As we're interested in calculating learning curves, from Eq. \eqref{Eq:learningCurveDef} it follows that we need to average $g^*$ and ${g^{*}}^{2}$ for all possible datasets, meaning $\left\langle g^*\right\rangle_{\mathcal{X}\sim\mu^N}$ and $\left\langle {g^{*}}^{2}\right\rangle_{\mathcal{X}\sim\mu^N}$.

    \pause

    Applying the replica trick to Eq. \eqref{Eq:Zprediction} we obtain

    \begin{equation}
        \label{Eq:Replica_f}
        g^*\left(x_*\right)=\lim_{M\to 0} M^{-1} \left.\frac{\delta  \mathcal{Z}^M\left[\alpha\right]} {\delta\alpha\left(x_*\right)}\right|_{\alpha=0}
    \end{equation}

    \pause

    For integer $M$ we get

    \begin{gather}
        \label{Eq:Z^M}
        \mathcal{Z}^{M}\left[\alpha\right]=\underbrace{\int\dots\int}_{M}\prod_{j=1}^{M}\mathcal{D} f_{j} \\ \nonumber \exp\left(-\frac{1}{2}\sum_{j=1}^{M}\left\Vert f_{j}\right\Vert _{K}^{2}-\sum_{j=1}^{M}\sum_{x_{i}\in\mathcal{X}}\frac{\left(f_{j}\left(x_{i}\right)-g\left(x_{i}\right)\right)^{2}}{2\sigma^{2}}+\sum_{j=1}^{M}\int\alpha\left(x\right)f_{j}\left(x\right)dx\right)
    \end{gather}

\end{frame}

\begin{frame}{Calculating Observables II}

    After averaging:

    \begin{gather}
        \left\langle \mathcal{Z}^M\left[\alpha\right]\right\rangle _{\mathcal{X}\sim\mu^N} = \underbrace{\int...\int}_{M}\prod_{j=1}^{M}\mathcal{D} f_{j} \\ \nonumber \exp\left(-\frac{1}{2}\sum_{j=1}^{M}\left\Vert f_{j}\right\Vert _{K}^{2}+\sum_{j=1}^{M}\int\alpha\left(x\right)f_{j}\left(x\right)dx\right) \\ \left\langle \exp\left(-\sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right)\right\rangle _{x\sim\mu}^{N}
    \end{gather}

    \pause

    But we want $\int\mathcal{D} f e^{-\mathrm{action}\left[f\right]}$!

\end{frame}

\begin{frame}{Calculating Observables III}

    \pause

    Poisson average:

    \begin{gather}
        \label{Eq:GCZM}
        \left\langle \mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta} =e^{-\eta}\sum_{N=0}^\infty \frac{\eta^N}{N!} \left\langle \mathcal{Z}^M\left[\alpha\right]\right\rangle _{\mathcal{X}\sim\mu^N} = \\ \nonumber
        =\underbrace{\int\dots\int}_{M}\mathcal{D} f_{1}\dots\mathcal{D} f_{M} \\ \nonumber \exp\left(-\frac{1}{2}\sum_{j=1}^{M}\left\Vert f_{j}\right\Vert _{K}^{2} +\eta\left\langle \exp\left(-\sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right)-1\right\rangle _{x\sim\mu} + \text{s.t.}\right)
    \end{gather}

    \pause

    \begin{equation}
        \label{Eq:fGC}
        \left\langle g^*\left(x_*\right)\right\rangle_{\eta} =
        \left.\frac{\delta}{\delta\alpha\left(x_*\right)}\right|_{\alpha=0}
        \lim_{M\to0}\frac{\left\langle\mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta}-1}{M} =
        \lim_{M\to0}\frac{\left.\frac{\delta}{\delta\alpha\left(x_*\right)}\right|_{\alpha=0}\left\langle\mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta}}{M}
    \end{equation}

\end{frame}

\begin{frame}{Calculating Observables IV}

    \pause

    For ${g^{*}}^{2}$ we need 2 replica indices:

    \pause

    \begin{equation}
        {g^{*}}^{2}\left(x_*\right)=\lim_{M\to 0} \lim_{W\to 0} M^{-1} W^{-1} \left.\frac{\delta  \mathcal{Z}^M\left[\alpha\right]}{\delta\alpha\left(x_*\right)}\right|_{\alpha=0} \left.\frac{\delta  \mathcal{Z}^{W}\left[\beta\right]}{\delta\beta\left(x_*\right)}\right|_{\beta=0}
    \end{equation}

    \pause

    Therefore

    \begin{equation}
        \label{Eq:<<f^2>>}
        \left\langle {g^{*}}^{2}\left(x_*\right)\right\rangle_{\eta} =
        \lim_{M\to0}\lim_{W\to0}\frac{\left.\frac{\delta^2}{\delta\alpha\left(x_*\right)\delta\beta\left(x_*\right)}\right|_{\alpha,\beta=0} \left\langle\mathcal{Z}^M\left[\alpha\right]\cdot \mathcal{Z}^W\left[\beta\right]\right\rangle_{\eta}}{MW}
    \end{equation}

\end{frame}

\begin{frame}{Equivalence Kernel as Free Theory I}

    \pause

    Expanding the data term in Eq. \eqref{Eq:GCZM} to first order we get

    \begin{gather}
        \eta\left\langle \exp\left(-\sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right)-1\right\rangle _{x\sim\mu} \approx
        - \eta\left\langle \sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right\rangle _{x\sim\mu}
    \end{gather}

    \pause

    and we see that at this order, the replicas do not interact so

    \begin{gather}
        \left\langle\mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta}=
        \\ \nonumber
        \left[\int Df\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}+\int\alpha\left(x\right)f\left(x\right)dx-\eta\left\langle \frac{\left(f\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right\rangle _{x\sim\mu_{x}}\right)\right]^{M}
        \\ \nonumber
        + O\left(1/\eta^2\right)= \\ \nonumber =\left(\mathcal{Z}_{EK}\left[\alpha\right]\right)^{M} + O\left(1/\eta^2\right)
    \end{gather}

\end{frame}

\begin{frame}{Equivalence Kernel as Free Theory II}

    where we defined

    \begin{gather}
        \mathcal{Z}_{EK}\left[\alpha\right] \equiv
        \\ \nonumber
        \int Df\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}+\int\alpha\left(x\right)f\left(x\right)dx-\frac{\eta}{2\sigma^{2}}\int d\mu_{x}\left(f\left(x\right)-g\left(x\right)\right)^{2}\right)
    \end{gather}

    \pause

    Therefore

    \begin{gather}
        \lim_{M\to0}\frac{\left\langle\mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta}-1}{M}=
        \lim_{M\to0}\frac{\left(\mathcal{Z}_{EK}\left[\alpha\right]\right)^{M}-1}{M} + O\left(1/\eta^2\right)=
        \\ \nonumber
        \log\left(\mathcal{Z}_{EK}\left[\alpha\right]\right) + O\left(1/\eta^2\right)
    \end{gather}

    which is quadratic in $f$, and therefore Gaussian.

\end{frame}

\begin{frame}{Equivalence Kernel as Free Theory III}

    \pause

    Denoting the average w.r.t $\mathcal{Z}_{EK}$ as $\langle\dots\rangle_0$, The mean of the distribution induced by $\mathcal{Z}_{EK}$ is

    \begin{gather}
        \left\langle f\left(x_*\right)\right\rangle_0 = \left.\frac{\delta\log\left(\mathcal{Z}_{EK}\left[\alpha\right]\right)}{\delta\alpha\left(x_{*}\right)}\right|_{\alpha=0}=\\ \nonumber
        =\frac{\int \mathcal{D} f\cdot f\left(x_{*}\right)\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}-\frac{\eta}{2\sigma^{2}}\int d\mu_{x}\left(f\left(x\right)-g\left(x\right)\right)^{2}\right)}{\int \mathcal{D} f\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}-\frac{\eta}{2\sigma^{2}}\int d\mu_{x}\left(f\left(x\right)-g\left(x\right)\right)^{2}\right)}= \\ \nonumber =\left.\arg\min\left[\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}+\frac{\eta}{2\sigma^{2}}\int d\mu_{x}\left(f\left(x\right)-g\left(x\right)\right)^{2}\right]\right|_{x_*} = g^*_{EK,\eta}\left(x_*\right)
    \end{gather}

    and that is exactly the result for the equivalence kernel as in Eq. \eqref{Eq:EK}, where $\eta$ replaces the data-set size $N$.

\end{frame}

\begin{frame}{Equivalence Kernel as Free Theory IV}

    \begin{gather}
        \label{Eq:cov0}
        \mathrm{Cov}_0\left[f\left(x\right),f\left(y\right)\right]  =
        \left\langle f\left(x\right)f\left(y\right)\right\rangle_0 -\left\langle f\left(x\right)\right\rangle_0\left\langle f\left(y\right)\right\rangle_0 = \left.\frac{\delta^2\log\left(\mathcal{Z}_{EK}\left[\alpha\right]\right)}{\delta\alpha\left(x\right)\delta\alpha\left(y\right)}\right|_{\alpha=0}                                                                               \\ \nonumber
        = \frac{\int Df\cdot f\left(x\right)f\left(y\right)\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}-\frac{\eta}{2\sigma^{2}}\int d\mu_{x}f^{2}\left(x\right)\right)}{\int Df\exp\left(-\frac{1}{2}\left\Vert f\right\Vert _{K}^{2}-\frac{\eta}{2\sigma^{2}}\int d\mu_{x}f^{2}\left(x\right)\right)} \\ \nonumber =\frac{\intop\prod_{i}df_{i}\cdot\sum_{i,j}f_{i}f_{j}\phi_{i}\left(x\right)\phi_{j}\left(y\right)\cdot\exp\left(-\frac{1}{2}\sum_{i}\left(\frac{1}{\lambda_{i}}+\frac{\eta}{\sigma^{2}}\right)f_{i}^{2}\right)}{\intop\prod_{i}df_{i}\exp\left(-\frac{1}{2}\sum_{i}\left(\frac{1}{\lambda_{i}}+\frac{\eta}{\sigma^{2}}\right)f_{i}^{2}\right)} \\ \nonumber
        =\sum_{i}\frac{\intop df_i\cdot f_i^{2}\cdot\exp\left(-\frac{1}{2}\left(\frac{1}{\lambda_{i}}+\frac{\eta}{\sigma^{2}}\right)f_i^{2}\right)}{\intop df_i\exp\left(-\frac{1}{2}\left(\frac{1}{\lambda_{i}}+\frac{\eta}{\sigma^{2}}\right)f_i^{2}\right)}\phi_{i}\left(x\right)\phi_{i}\left(y\right)      \\ \nonumber =\sum_{i}\left(\frac{1}{\lambda_{i}}+\frac{\eta}{\sigma^{2}}\right)^{-1}\phi_{i}\left(x\right)\phi_{i}\left(y\right)
    \end{gather}

\end{frame}

\begin{frame}{Equivalence Kernel as Free Theory IV}

    \pause

    Being Gaussian, we can regard $\mathcal{Z}_{EK}$ as our free, non-interacting theory, and to pertubatively calculate the corrections in order to approximate Eq. \eqref{Eq:fGC}.

    \pause

    Now, Eq. \eqref{Eq:fGC} can be re-written as

    \begin{equation}
        \label{Eq:fGC_EK}
        \left\langle g^*\left(x_{*}\right)\right\rangle_{\eta}=
        \lim_{M\to0}\frac{\left.\frac{\delta}{\delta\alpha\left(x_*\right)}\right|_{\alpha=0}\left\langle\mathcal{Z}^M\left[\alpha\right]\right\rangle_{\eta}}{M\cdot\left(\mathcal{Z}_{EK}\left[0\right]\right)^{M}}
    \end{equation}

    \pause

    Eq. \eqref{Eq:<<f^2>>} can be re-written as

    \begin{equation}
        \label{Eq:<<f^2>>EK}
        \left\langle {g^{*}}^{2}\left(x_*\right)\right\rangle_{\eta} =
        \lim_{M\to0}\lim_{W\to0}\frac{\left.\frac{\delta^2}{\delta\alpha\left(x_*\right)\delta\beta\left(x_*\right)}\right|_{\alpha,\beta=0} \left\langle\mathcal{Z}^M\left[\alpha\right]\cdot \mathcal{Z}^W\left[\beta\right]\right\rangle_{\eta}}{MW\cdot\left(\mathcal{Z}_{EK}\left[0\right]\right)^{M+W}}
    \end{equation}

\end{frame}

\begin{frame}{Next Order Correction I}

    We now wish to perform the first order correction to the free theory.

    \pause

    Expanding Eq. \eqref{Eq:GCZM} to the next order (keeping terms up to $O\left(1/\eta^2\right)$) we get

    \begin{gather}
        \label{Eq:pertZ}
        \left\langle \mathcal{Z}^{M}\left[\alpha\right]\right\rangle_{\eta} =\underbrace{\int...\int}_{M\,\,\mathrm{times}}\mathcal{D} f_{1}\dots\mathcal{D} f_{M}\\ \nonumber\exp\left(-\frac{1}{2}\sum_{j=1}^{M}\left\Vert f_{j}\right\Vert _{K}^{2}+\sum_{j=1}^{M}\int\alpha\left(x\right)f_{j}\left(x\right)dx+\eta\left\langle -\sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right\rangle _{x\sim\mu_{x}}\right)\\ \nonumber\exp\left(\frac{\eta}{2}\left\langle \left(\sum_{j=1}^{M}\frac{\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right)^{2}\right\rangle _{x\sim\mu_{x}}\right) + O\left(1/\eta^3\right) = \\ \nonumber
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction II}

    \begin{gather}
        =\underbrace{\int\dots\int}_{M\,\,\mathrm{times}}\mathcal{D} f_{1}\dots\mathcal{D} f_{M}\\ \nonumber\exp\left(\sum_{i=1}^{M}\left(-\frac{1}{2}\left\Vert f_{i}\right\Vert _{K}^{2}+\int\alpha\left(x\right)f_{i}\left(x\right)dx-\eta\left\langle \frac{\left(f_{i}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right\rangle _{x\sim\mu}\right)\right)\\ \nonumber\exp\left(\frac{\eta}{8\sigma^{4}}\sum_{i=1}^{M}\sum_{j=1}^{M}\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}\cdot\left(f_{i}\left(x\right)-g\left(x\right)\right)^{2}\right) + O\left(1/\eta^3\right) =
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction III}

    \begin{gather}
        = \underbrace{\int\dots\int}_{M\,\,\mathrm{times}}\mathcal{D} f_{1}\dots\mathcal{D} f_{M}\\ \nonumber\exp\left(\sum_{i=1}^{M}\left(-\frac{1}{2}\left\Vert f_{i}\right\Vert _{K}^{2}+\int\alpha\left(x\right)f_{i}\left(x\right)dx-\eta\left\langle \frac{\left(f_{i}\left(x\right)-g\left(x\right)\right)^{2}}{2\sigma^{2}}\right\rangle _{x\sim\mu}\right)\right)\\ \nonumber\left(1+\frac{\eta}{8\sigma^{4}}\sum_{i=1}^{M}\sum_{j=1}^{M}\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}\cdot\left(f_{i}\left(x\right)-g\left(x\right)\right)^{2}\right) + O\left(1/\eta^3\right)
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction IV}

    \pause

    We now wish to calculate the correction to $\left\langle {g^*}\right\rangle_{\eta}$ given by Eq. \eqref{Eq:pertZ}. \pause From Eq. \eqref{Eq:fGC_EK} we get

    \begin{gather}
        \left\langle {g^*}\left(x_*\right)\right\rangle_{\eta} = g^*_{EK,\eta}\left(x_*\right)+ \\ \nonumber
        \lim_{M\to0}\frac{1}{M}\frac{\eta}{8\sigma^{4}}\intop d\mu_{x}\left\langle \sum_{j=1}^{M}\sum_{l=1}^{M}\sum_{i=1}^{M}\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}\cdot\left(f_{l}\left(x\right)-g\left(x\right)\right)^{2}f_{i}\left(x_*\right)\right\rangle _{0} \\ \nonumber + O\left(1/\eta^3\right)
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction V}

    Simplifying the average of the multiple sums we get

    \begin{gather}
        \left\langle \sum_{j=1}^{M}\sum_{l=1}^{M}\sum_{i=1}^{M}\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}\cdot\left(f_{l}\left(x\right)-g\left(x\right)\right)^{2}f_{i}\left(x_*\right)\right\rangle _{0} =\\ \nonumber
        =M\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{4}f\left(x_*\right)\right\rangle _{0} \\ \nonumber+ 2M\left(M-1\right)\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}\right\rangle _{0}\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}f\left(x_*\right)\right\rangle _{0} +
        \\ \nonumber
        M\left( M - 1 \right)\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{4}\right\rangle _{0}\left\langle f\left(x_*\right)\right\rangle _{0} \\ \nonumber
        +M\left(M-1\right)\left(M-2\right)\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}\right\rangle _{0}^{2}\left\langle f\left(x_*\right)\right\rangle _{0}
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction VI}

    \pause

    Since $f$ has a Gaussian distribution ($f\sim \mathcal{Z}_{EK}$), such averages can be calculated using Feynman diagrams.

    \pause

    \begin{block}{Feyman rules}
        \begin{itemize}
            \item\pause $f\left(x\right)-g\left(x\right)$ $\to$
                  \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-0.5,xscale=0.5]
                      \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (309,41) -- (327.5,41) -- (327.5,59.5) -- (309,59.5) -- cycle ;
                  \end{tikzpicture}
            \item\pause $f\left(x_*\right)$ $\to$
                  \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-0.5,xscale=0.5]
                      \draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ,draw opacity=1 ][fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (368.02,49.98) .. controls (368.02,44.46) and (372.5,39.98) .. (378.02,39.98) .. controls (383.54,39.98) and (388.02,44.46) .. (388.02,49.98) .. controls (388.02,55.5) and (383.54,59.98) .. (378.02,59.98) .. controls (372.5,59.98) and (368.02,55.5) .. (368.02,49.98) -- cycle ;
                  \end{tikzpicture}
            \item\pause Non-centered theory ($\left\langle f \right\rangle_0= g^*_{EK,\eta}\neq 0$) $\to$ free edges allowed
        \end{itemize}
    \end{block}

    \pause

    \begin{alertblock}{Note}
        Note that since we divide by $M$ and take the limit $M\to0$, we do not care about diagrams which are not connected to $f\left(x_*\right)$ since they scale as $M^2$.
    \end{alertblock}

\end{frame}

\begin{frame}{Next Order Correction VII}

    Calculating the averages we get
    \begin{gather}
        \left\langle \left(f\left(x\right)-g\left(x\right)\right)^{4}f\left(x_*\right)\right\rangle _{0} =
    \end{gather}
    \tikzset{every picture/.style={line width=0.75pt}} %set default line width to 0.75pt        
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-0.7,xscale=0.7]
        %uncomment if require: \path (0,63.3125); %set diagram left start at 0, and has height of 63.3125

        %Flowchart: Connector [id:dp9563916183651779] 
        \draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ,draw opacity=1 ][fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (193.02,29.98) .. controls (193.02,24.46) and (197.5,19.98) .. (203.02,19.98) .. controls (208.54,19.98) and (213.02,24.46) .. (213.02,29.98) .. controls (213.02,35.5) and (208.54,39.98) .. (203.02,39.98) .. controls (197.5,39.98) and (193.02,35.5) .. (193.02,29.98) -- cycle ;
        %Shape: Square [id:dp45680349836434075] 
        \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (134,21) -- (152.5,21) -- (152.5,39.5) -- (134,39.5) -- cycle ;
        %Shape: Block Arc [id:dp5288295482962739] 
        \draw   (133.88,31.18) .. controls (128.32,31.11) and (123.82,26.58) .. (123.82,21) .. controls (123.82,15.38) and (128.38,10.82) .. (134,10.82) .. controls (139.59,10.82) and (144.13,15.34) .. (144.18,20.92) -- (144.18,20.92) .. controls (144.13,15.34) and (139.59,10.82) .. (134,10.82) .. controls (128.38,10.82) and (123.82,15.38) .. (123.82,21) .. controls (123.82,26.58) and (128.32,31.11) .. (133.88,31.18) -- cycle ;
        %Straight Lines [id:da15350662350882494] 
        \draw    (143.25,30.25) -- (203.02,29.98) ;


        %Straight Lines [id:da2710375805255476] 
        \draw    (143.25,30.25) -- (143.25,51.25) ;


        %Flowchart: Connector [id:dp9886894139272266] 
        \draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ,draw opacity=1 ][fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (348.02,29.98) .. controls (348.02,24.46) and (352.5,19.98) .. (358.02,19.98) .. controls (363.54,19.98) and (368.02,24.46) .. (368.02,29.98) .. controls (368.02,35.5) and (363.54,39.98) .. (358.02,39.98) .. controls (352.5,39.98) and (348.02,35.5) .. (348.02,29.98) -- cycle ;
        %Shape: Square [id:dp08030119002696456] 
        \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (289,21) -- (307.5,21) -- (307.5,39.5) -- (289,39.5) -- cycle ;
        %Straight Lines [id:da4869114855582213] 
        \draw    (277.5,30.31) -- (358.02,29.98) ;


        %Straight Lines [id:da9392155938489644] 
        \draw    (298.5,9.31) -- (298.25,50.25) ;



        % Text Node
        \draw (244,28.31) node   {$+$};
        % Text Node
        \draw (404,28.31) node   {$+$};
        % Text Node
        \draw (519,29.31) node  [align=left] {disconnected diagrams};
        % Text Node
        \draw (77,27.31) node   {$=$};
        % Text Node
        \draw (637,27.31) node   {$=$};


    \end{tikzpicture}
    \begin{gather*}=12\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right)\mathrm{Var}_0 \left[f\left(x\right)\right]\mathrm{Cov}_0 \left[f\left(x\right),f\left(x_{*}\right)\right] \\ \nonumber
        + 4\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right)^{3}\mathrm{Cov}_0 \left[f\left(x\right),f\left(x_{*}\right)\right] \\ \nonumber
        + \text{disconnected diagrams}
    \end{gather*}

\end{frame}

\begin{frame}{Next Order Correction VIII}

    \begin{gather}
        \left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}\right\rangle _{0}\left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}f\left(x_*\right)\right\rangle _{0} =
    \end{gather}
    \begin{tikzpicture}[x=0.75pt,y=0.75pt,yscale=-0.7,xscale=0.7]
        %uncomment if require: \path (0,83.02082824707031); %set diagram left start at 0, and has height of 83.02082824707031

        %Shape: Square [id:dp15231147729912387] 
        \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (133,29) -- (151.5,29) -- (151.5,47.5) -- (133,47.5) -- cycle ;
        %Straight Lines [id:da15693743396440185] 
        \draw    (142.5,18.31) -- (142.25,59.25) ;


        %Shape: Square [id:dp5891709567954524] 
        \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (193,29) -- (211.5,29) -- (211.5,47.5) -- (193,47.5) -- cycle ;
        %Flowchart: Terminator [id:dp7432981684933464] 
        \draw   (221.5,24.14) -- (221.5,51.57) .. controls (221.5,55.13) and (217.13,58.02) .. (211.75,58.02) .. controls (206.37,58.02) and (202,55.13) .. (202,51.57) -- (202,24.14) .. controls (202,20.58) and (206.37,17.69) .. (211.75,17.69) .. controls (217.13,17.69) and (221.5,20.58) .. (221.5,24.14) -- cycle ;
        %Flowchart: Connector [id:dp014605632831485638] 
        \draw  [color={rgb, 255:red, 0; green, 0; blue, 0 }  ,draw opacity=1 ][fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (351.02,37.98) .. controls (351.02,32.46) and (355.5,27.98) .. (361.02,27.98) .. controls (366.54,27.98) and (371.02,32.46) .. (371.02,37.98) .. controls (371.02,43.5) and (366.54,47.98) .. (361.02,47.98) .. controls (355.5,47.98) and (351.02,43.5) .. (351.02,37.98) -- cycle ;
        %Shape: Square [id:dp2808260818485815] 
        \draw  [fill={rgb, 255:red, 0; green, 0; blue, 0 }  ,fill opacity=1 ] (292,29) -- (310.5,29) -- (310.5,47.5) -- (292,47.5) -- cycle ;
        %Straight Lines [id:da43040206025750916] 
        \draw    (280.5,38.31) -- (361.02,37.98) ;



        % Text Node
        \draw (172,35.31) node   {$+$};
        % Text Node
        \draw (118,37.02) node [scale=2.488]  {$($};
        % Text Node
        \draw (243,37.02) node [scale=2.488]  {$)$};
        % Text Node
        \draw (265,37.02) node [scale=2.488]  {$($};
        % Text Node
        \draw (596,37.02) node [scale=2.488]  {$)$};
        % Text Node
        \draw (403,36.31) node   {$+$};
        % Text Node
        \draw (506,38.31) node  [align=left] {disconnected diagrams};
        % Text Node
        \draw (637,37.31) node   {$=$};
        % Text Node
        \draw (77,37.31) node   {$=$};


    \end{tikzpicture}
    \begin{gather*}
        =2\mathrm{Cov}_0 \left[f\left(x\right),f\left(x_{*}\right)\right]\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right)^{3} \\ \nonumber
        +2\mathrm{Var}_0 \left[f\left(x\right)\right]\mathrm{Cov}_0 \left[f\left(x\right),f\left(x_{*}\right)\right]\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right) \\ \nonumber
        + \text{ disconnected diagrams}
    \end{gather*}
    \begin{gather}
        \left\langle \left(f\left(x\right)-g\left(x\right)\right)^{4}\right\rangle _{0}\left\langle f\left(x_*\right)\right\rangle _{0} = \text{disconnected diagrams}
    \end{gather}
    \begin{gather}
        \left\langle \left(f\left(x\right)-g\left(x\right)\right)^{2}\right\rangle _{0}^{2}\left\langle f\left(x_*\right)\right\rangle _{0}= \text{disconnected diagrams}
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction IX}

    \pause

    Taking the limit $M\to0$ and summing everything together we get

    \begin{gather}
        \label{Eq:Relevant2f2}
        \lim_{M\to0}\frac{1}{M}\left\langle \sum_{j=1}^{M}\sum_{l=1}^{M}\sum_{i=1}^{M}\left(f_{j}\left(x\right)-g\left(x\right)\right)^{2}\cdot\left(f_{l}\left(x\right)-g\left(x\right)\right)^{2}f_{i}\left(x_*\right)\right\rangle _{0}
        =\\ \nonumber
        8\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right)\mathrm{Var}_{0}\left[f\left(x\right)\right]\mathrm{Cov}_{0}\left[f\left(x\right),f\left(x_*\right)\right]
    \end{gather}

    \pause

    so finally

    \begin{gather}
        g^*_{SL,\eta}\left(x_{*}\right) = -
        \frac{\eta}{\sigma^{4}}\intop d\mu_{x}\left(g^*_{EK,\eta}\left(x\right)-g\left(x\right)\right)\mathrm{Var}_{0}\left[f\left(x\right)\right]\mathrm{Cov}_{0}\left[f\left(x\right),f\left(x_*\right)\right]
    \end{gather}

\end{frame}

\begin{frame}{Next Order Correction X}

    \pause

    Substituting the expressions for the free variance and covariance (Eq. \eqref{Eq:cov0}) we get

    \begin{align}
        \label{Eq:f_nextOrder}
        g^*_{SL,\eta}\left(x_*\right) & = \nonumber -\frac{\eta}{\sigma^{4}}\sum_{i,j,k} \Lambda_{i,j,k}
        g_{i}\phi_{j}\left(x_{*}\right) \intop d\mu_{x}\phi_{i}\left(x\right)\phi_{j}\left(x\right)\phi_{k}^{2}\left(x\right)                                                                                                               \\
        \Lambda_{i,j,k}               & = \frac{\frac{\sigma^{2}}{\eta}}{\lambda_{i}+\frac{\sigma^{2}}{\eta}}\left(\frac{1}{\lambda_{j}}+\frac{\eta}{\sigma^{2}}\right)^{-1}\left(\frac{1}{\lambda_{k}}+\frac{\eta}{\sigma^{2}}\right)^{-1}
    \end{align}

    \pause

    Similar derivation was performed for $\left\langle {g^{*}}^{2}\left(x_{*}\right)\right\rangle_{\eta}$ to give

    \begin{gather}
        \label{Eq:f2_nextOrder}
        \left\langle {g^{*}}^{2}\left(x_{*}\right)\right\rangle_{\eta} =
        {g^{*}_{EK,\eta}}^{2}\left(x_*\right) + 2\cdot g^*_{EK,\eta}\left(x_*\right) \cdot g^*_{SL,\eta}\left(x_*\right) + O\left(1/\eta^3\right)
    \end{gather}

\end{frame}

\begin{frame}{Dot-product Kernels I}

    \pause

    \begin{block}{Definition}
        A \emph{rotationally invariant kernel} is a positive semi-definite function $K: S ^{d-1}\times S ^{d-1}\to\mathbb{R}$, such that for every orthogonal transformation $O\in \mathrm{O}\left(d\right)$ it holds that
        \begin{equation}
            K\left(Ox,Oy\right) = K\left(x,y\right)
        \end{equation}
    \end{block}

    \pause

    Let us proof that the NNGP kernels associated with any network whose first layer is fully-connected, are rotationally invariant.

\end{frame}

\begin{frame}{Dot-product Kernels II}

    \begin{itemize}
        \item\pause $w$ and $b$ - The weights and biases of the first layer respectively
        \item\pause $h_w\left(x\right)$ - The output vector of the first layer $[h_w\left(x\right)]_i = \phi(\sum_j w_{ij}x_j + b)$ where $x_j$ is the $j$'th component of the input vector $x$
        \item\pause $w'$ - The rest of the weights (and biases)
        \item\pause $z_{w'}\left(h\right)$ The output of the rest of the network given $h$.
    \end{itemize}

    \pause

    The covariance function of NNGPs is given by

    \begin{align}
        K\left(x,y\right) & = \int dw dw' P_{nd}(w,b,w') z_{w'}(h_w\left(x\right))z_{w'}(h_w\left(y\right))
    \end{align}

\end{frame}

\begin{frame}{Dot-product Kernels III}

    \pause

    For some orthogonal transformation $O$ over the input space we get

    \begin{align}
        K(Ox,Oy) & = \int dw dw' P_{nd}(w,b,w') z_{w'}(h_w(Ox))z_{w'}(h_w(Oy))
        \\ \nonumber
                 & = \int dw dw' P_{nd}(w,b,w') z_{w'}(h_{O^Tw}\left(x\right))z_{w'}(h_{O^Tw}\left(y\right))
        \\ \nonumber
                 & = \int dw dw' P_{nd}(Ow,b,w') z_{w'}(h_{w}\left(x\right))z_{w'}(h_{w}\left(y\right)
        \\ \nonumber
                 & = \int dw dw' P_{nd}(w,b,w') z_{w'}(h_{w}\left(x\right))z_{w'}(h_{w}\left(y\right) = K\left(x,y\right)
    \end{align}

    where the second equality uses the definition of $h_w\left(x\right)$, the third results from an orthogonal change of integration variable $w \rightarrow O^Tw$, and the forth is a property is due to the fact that since $P_{nd}$ is a rotationally invariant Gaussian distribution $P_{nd}\left(W\right)\propto e^{-\frac{1}{2\sigma_w}\sum_i w_i^2}$.

\end{frame}

\begin{frame}{Dot-product Kernels IV}

    \pause

    Assuming a uniformly distributed data, we wish to find the eigenvalues of a dot-product kernel. Let us assume a general dot-product kernel, given as

    \begin{equation}
        K\left(x,y\right)=\sum_{n=0}^{\infty}b_{n}\left(x\cdot y\right)^{n}
    \end{equation}

    \pause

    It can be diagonalized by the hyperspherical harmonics $Y_{lm}$. A long, technical derivation which involves the Gegenbauer polynomial gives

    \begin{equation}
        \label{Eq:RotInvEigVals}
        \lambda_{l}=\frac{\Gamma\left(\frac{d}{2}\right)}{\sqrt{\pi}\cdot2^{l}}\sum_{s=0}^{\infty}b_{l+2s}\frac{\left(l+2s\right)!\cdot\Gamma\left(s+\frac{1}{2}\right)}{\left(2s\right)!\cdot\Gamma\left(l+s+\frac{d}{2}\right)}
    \end{equation}

\end{frame}

\begin{frame}{Applying the Results to Dot-product Kernels I}

    \pause

    Eq. \eqref{Eq:cov0} becomes

    \begin{gather}
        \mathrm{Cov}_0\left[f\left(x\right),f\left(y\right)\right]=\sum_{l}\left(\frac{1}{\lambda_{l}}+\frac{\eta}{\sigma^{2}}\right)^{-1}\sum_{m}Y_{lm}\left(x\right)Y_{lm}\left(y\right)
    \end{gather}

    and the variance is

    \begin{gather}
        \mathrm{Var}_0\left[f\left(x\right)\right]=\sum_{l}\left(\frac{1}{\lambda_{l}}+\frac{\eta}{\sigma^{2}}\right)^{-1}\underbrace{\sum_{m}Y_{lm}^2\left(x\right)}_{\deg\left(l\right)} \equiv C_{K,\eta/\sigma^2}
    \end{gather}

    which is a constant (independent of $x$ and $y$).

\end{frame}

\begin{frame}{Applying the Results to Dot-product Kernels II}

    For the sub-leading correction, Eq. \eqref{Eq:f_nextOrder} becomes

    \begin{equation}
        \label{Eq:f_nextOrder_rot}
        g^{*}_{SL,\eta}(x_{*}) = - \sum_{l,m}\frac{\eta^{-1} \lambda_l C_{K,\eta/\sigma^2}}{(\lambda_l+\sigma^2/\eta)^2}g_{lm} Y_{lm}\left(x_*\right)
    \end{equation}

    and notably cross-talk between the eigenfunctions has been eliminated at this order since $\sum_m Y_{lm}^2\left(x\right)=\deg\left(l\right)$, yielding $\sum_{\tilde{m}}\int d\mu_x Y_{lm}\left(x\right) Y_{l'm'}\left(x\right)Y^2_{
        \tilde{l}\tilde{m}}\left(x\right) = \deg(\tilde l) \delta_{ll'}\delta_{mm'}$.

\end{frame}
