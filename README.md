# MSc Presentation

[![View PDF](https://img.shields.io/badge/View-PDF-green)](https://omryco.gitlab.io/msc-presentation/main.pdf)

Based on ['Heverlee' Beamer theme](https://www.overleaf.com/latex/templates/heverlee-beamer-theme/jbghgbxpqzzs).
